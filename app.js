//Estoy importando colors
require('colors');
const argv = require('yargs').argv;


//Aca se trae ese archivo que se esta exportando
const { generarArchivo } = require('./helpers/multiplicar');



/*
const argv = require('yargs').argv;

console.log(process.argv);
console.log(argv);

console.log('base: yargs', argv.base);
*/



/*
DESDE ACA  PUEDES REVISAR LO DEL LA EXPORTACION PRINCIPAL
// const base = 7;

// console.log(process.argv);    Esto solo trae 2 valores
//Desestructuracion de array
//Si no pasa una variable va a tomar esto
const [,,arg3='base=9'] = process.argv;
const [, base] = arg3.split('=');
//lo que hace esto es que separa cadena y asi creo es que tomarias el numero 9
console.log(base);

//Es un proceso de paso de variables como readline y pasas una variable con  node app.js --base=5

generarArchivo(base)
    .then(nombreArchivo => console.log(nombreArchivo, 'creado'))
    .catch(err => console.log(err));
*/
generarArchivo(argv.base)
    .then(nombreArchivo => console.log(nombreArchivo.rainbow, 'creado'))
    .catch(err => console.log(err));








































// //tODO ESTE CODIGO ERA LO QUE HABIAMOS PUESTO EN EL PRINCIPIO





// //Estoy importando el paquete file system (fs)

// const fs = require('fs');

// //Try y CAthc es un control de todo el proceso
// try{
//     const base = 8;
//     let salida = '';

//     console.log('===============');
//     console.log(`Tabla del ${base}`);
//     console.log('===============');

//     for (let i = 1; i <= 10; i++) {
//         salida += `${base} x ${i} = ${base*i} \n`;
//     }
//     console.log(salida);

//     //El primer parametro va  a ser el nombre del archivo
//     //El segundo parametro es el contenido a escribir
//     //Tercer Parametro captura el error
//     //Si no tiene un error es undefined o null


//     //DESCOMENTAR ESTO PORQUE MMM ES EL PROCESO DE CREADO DE UN ARCHIVO
//     // fs.writeFile(`tabla-${base}.txt`,salida, (err)=>{
//     //     if (err)throw err;
//     //     //Throw lo va a forzar sirve para forzar errores y de ahi ya no desciende hacia abajo
//     //     console.log(`table-${base} creado`);
//     // });

//     //Este Proceso hace lo mismo de arriba pero lo mejora
//     fs.writeFileSync(`tabla-${base}.txt`,salida);
//     console.log(`tabla-${base}.txt creado`);
// } catch(error) {
//     throw error;
// }