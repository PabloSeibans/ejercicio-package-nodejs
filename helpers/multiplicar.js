//Estoy importando el paquete file System (fs)
require('colors');

const fs = require('fs');

const crearArchivo = async (base = 5)=> {
    try{
        let salida = '';
    
        console.log('==============='.green);
        console.log(`Tabla del ${base}`.green);
        console.log('==============='.green);
    
        for (let i = 1; i <= 10; i++) {
            salida += `${base} x ${i} = ${base*i} \n`;
        }
        console.log(salida);
    
        //El primer parametro va  a ser el nombre del archivo
        //El segundo parametro es el contenido a escribir
        //Tercer Parametro captura el error
        //Si no tiene un error es undefined o null
    
    
        //DESCOMENTAR ESTO PORQUE MMM ES EL PROCESO DE CREADO DE UN ARCHIVO
        // fs.writeFile(`tabla-${base}.txt`,salida, (err)=>{
        //     if (err)throw err;
        //     //Throw lo va a forzar sirve para forzar errores y de ahi ya no desciende hacia abajo
        //     console.log(`table-${base} creado`);
        // });
    
        //Este Proceso hace lo mismo de arriba pero lo mejora
        fs.writeFileSync(`tabla-${base}.txt`,salida);
        return `tabla-${base}.txt`;
    } catch(error) {
        throw error;
    }
};
//Exportamos el archivo
//Tiene que estar en un formato json
module.exports={
    generarArchivo: crearArchivo
};